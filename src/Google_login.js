import React from 'react';
import {GoogleLogin,GoogleLogout, useGoogleLogin } from 'react-google-login';
import {Logout} from './Google_logout';
const onFailurehandle = (response) => {
    console.log("login fail!");
  }
 const onSuccessfulhandle=()=>
 {
   console.log("Google login sucessful!")
 } 

  export const Login=()=> {

    return (
    <>
  
  <GoogleLogin
      clientId="889637176355-bcsc0k6d17364djb5b8bqvke2rcc5i5u.apps.googleusercontent.com"
      buttonText="Login with google"
      // onSuccess={responseGoogle}
      onSuccess={onSuccessfulhandle}
      onFailure={onFailurehandle }
      cookiePolicy={'single_host_origin'}
    />
  <Logout/>
    </>
    );
  }